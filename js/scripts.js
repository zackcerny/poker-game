function Card(suit,cardValue){
	this.suit = suit;
	this.cardValue = cardValue;
	this.cardClass = "" + suit + cardValue;
	this.isHeld = false;
	this.svg = "images/" + suit + "s/" + cardValue + ".svg";
}

//Deck object
var deck = {
	cards: [],
	createDeck: function(){
		for(var i = 0; i<52; i++){

			if(i < 13){
				this.cards[i] = new Card("heart",i+1);
			} else if(i < 26){
				this.cards[i] = new Card("diamond",(i+1)-13);
			} else if(i < 39){
				this.cards[i] = new Card("club",(i+1)-26);
			} else{
				this.cards[i] = new Card("spade",(i+1)-39);
			}
			
		}
	},

	shuffle: function(){
		for (var i = this.cards.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = this.cards[i];
        this.cards[i] = this.cards[j];
        this.cards[j] = temp;
	    }
	}
};

//Player's Hand
var hand = {
	score: 0,
	newTurn: false,
	cards: [],

	newHand: function(deck){ //create's a new hand from the top 5 cards of the deck
			for(var i =0; i<5; i++){
				this.cards[i] = deck.cards.shift();
			}
			console.log(this.cards);
			this.showCards();
			this.unholdAll();
	},

	showCards: function(){ //shows the cards using jQuery
		for(var i = 0; i<this.cards.length; i++){
			$(".card-" +(i+1) ).css('background-image','url('+ this.cards[i].svg+')');
			$(".card-" + (i+1) ).attr("data-card",this.cards[i].cardClass);
		}
	},

	holdCard: function(card){
		for(var i = 0; i<this.cards.length; i++){
			if(card == this.cards[i].cardClass){
				if(this.cards[i].isHeld === false){
					this.cards[i].isHeld = true;
					$("div[data-card='"+card+"']").find('.hold').text("Held");
				} else{
					this.cards[i].isHeld = false;
					$("div[data-card='"+card+"']").find('.hold').text("");
				}
			}
		}
	},

	unholdAll: function(){
		for(var i = 0; i<this.cards.length; i++){
			this.cards[i].isHeld = false;
			$("div[data-card='"+this.cards[i].cardClass+"']").find('.hold').text("");
		}
	},

	changeCards: function(){
		for(var i = 0; i<this.cards.length; i++){
			if(this.cards[i].isHeld === false){
				this.cards[i] = deck.cards.shift();
			}
		}
		this.showCards();
	},

	isTwoOfAKind: function(){ 
		var count = 0;
		for (var i = 0; i<this.cards.length; i++){
			for(var z = 0; z<this.cards.length; z++){
				if(this.cards[i].cardValue == this.cards[z].cardValue){
					count++;
				}
			}
			if(count == 2){
				return true;
			} else{
				count = 0;
			}
		}
	},

	isThreeOfAKind: function(){ 
		var count = 0;
		for (var i = 0; i<this.cards.length; i++){
			for(var z = 0; z<this.cards.length; z++){
				if(this.cards[i].cardValue == this.cards[z].cardValue){
					count++;
				}
			}
			if(count == 3){
				return true;
			} else{
				count = 0;
			}
		}
	},

	isFourOfAKind: function(){ 
		var count = 0;
		for (var i = 0; i<this.cards.length; i++){
			for(var z = 0; z<this.cards.length; z++){
				if(this.cards[i].cardValue == this.cards[z].cardValue){
					count++;
				}
			}
			if(count == 4){
				return true;
			} else{
				count = 0;
			}
		}
	},

	isStraight: function(){
		var orderedCards = [];
		var orderedCards2 = [];
		var count = 0;
		var count2 = 0;
		for(var i = 0; i<this.cards.length; i++){
			orderedCards.push(this.cards[i].cardValue);
		}

		orderedCards.sort(function(a, b){return a-b});

		for(var a = 0; a<orderedCards.length; a++){
			if(orderedCards[a] === 1){
				orderedCards2.push(14);
			} else{
				orderedCards2.push(orderedCards[a]);
			}
		}

		orderedCards2.sort(function(a,b){return a-b});

		for(var z = 0; z<orderedCards.length-1; z++){
			if(orderedCards[z] === orderedCards[z+1]-1){
				count++;
			}
		}

		for(var b = 0; b<orderedCards2.length-1; b++){
			if(orderedCards2[b] === orderedCards2[b+1]-1){
				count2++;
			}
		}

		if(count === 4 || count2 === 4){
			return true;
		} else{
			return false;
		}
	},

	isFullHouse: function(){
		var countX = 0;
		var countY = 0;

		for(var i = 0; i<this.cards.length; i++){
			if(this.cards[0].cardValue == this.cards[i].cardValue){
				countX++;
			}
		}

		for(var z = 0; z<this.cards.length; z++){
			for(var y = 0; y<this.cards.length; y++){
				if(this.cards[z].cardValue == this.cards[y].cardValue && this.cards[z].cardValue !== this.cards[0].cardValue){
					countY++;
				}
			}
			if(countX == 2 && countY == 3){
				return true;
			} else if (countX == 3 && countY == 2){
				return true;
			} else{
				countY = 0; 
			}
		}
	},

	isTwoPair: function(){
		var countX = 0;
		var countY = 0;

		for(var i = 0; i<this.cards.length; i++){
			if(this.cards[0].cardValue == this.cards[i].cardValue){
				countX++;
			}
		}

		for(var z = 0; z<this.cards.length; z++){
			for(var y = 0; y<this.cards.length; y++){
				if(this.cards[z].cardValue == this.cards[y].cardValue && this.cards[z].cardValue !== this.cards[0].cardValue){
					countY++;
				}
			}
			if(countX == 2 && countY == 2){
				return true;
			}
			else{
				countY = 0; 
			}
		}
	},

	isFlush: function(){
		var count = 0;
		for(var i=0; i<this.cards.length-1; i++){
			if(this.cards[i].suit == this.cards[i+1].suit){
				count++;
			}
		}

		if(count === 4){
			return true;
		} else{
			return false;
		}
	},

	checkScore: function(){
		if(this.isStraight() && this.isFlush() ){
			this.score+=1000;
			console.log("+1000! STRAIGHT FLUSH!");
			$('.typeOfHand').html('+1000! STRAIGHT FLUSH!');
		}	

		else if( this.isFourOfAKind() ){
			this.score+=500;
			console.log("+250! Four of a Kind!");
			$('.typeOfHand').html('+250! Four of a Kind!');
		}

		else if(this.isFullHouse()){
			this.score+=150;
			console.log("+150! Full House!");
			$('.typeOfHand').html('+150! Full House!');
		}

		else if(this.isFlush() ){
			this.score+=125;
			console.log("+125! Flush!");
			$('.typeOfHand').html('+125! Flush!');
		}

		else if(this.isStraight() ){
			this.score+=100;
			console.log("+100! Straight!");
			$('.typeOfHand').html('+100! Straight!');
		}

		else if( this.isThreeOfAKind() ){
			this.score+=30;
			console.log("+30! Three of a Kind!");
			$('.typeOfHand').html('+30! Three of a Kind!');
		} 

		else if(this.isTwoPair() ){
			this.score+=50;
			console.log("+50! Two Pair!");
			$('.typeOfHand').html('+50! Two Pair!');
		}

		else if( this.isTwoOfAKind() ){
			this.score+=10;
			console.log("+10! Two of a Kind!");
			$('.typeOfHand').html('+10! Two of a Kind!');
		}

		$('.score').text(this.score);
	}

};


//Start the actual Game
$(document).ready(function(){

	deck.createDeck();
	deck.shuffle();

	hand.newHand(deck);

	$('.card').click(function(){ //Hold Card functionality.
		console.log($(this).attr('data-card'));
		var heldCard = $(this).attr('data-card');
		hand.holdCard(heldCard);
	});

	$('.new-cards').click(function(){
		
		if(deck.cards.length < 5){
			deck.createDeck();
			deck.shuffle();
		}

		$('.deckSize').text(deck.cards.length);

		if( hand.newTurn ){
			hand.newHand(deck);
			hand.newTurn = false;
			$('.new-cards').text("Play with Held Cards");
			$('.typeOfHand').html('');
		} else{
			hand.changeCards();
			hand.checkScore();
			hand.newTurn = true;
			$('.new-cards').text("new hand");
		}

	});

});